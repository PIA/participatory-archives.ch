let acc_contents, acc_triggers, coll_contents, coll_triggers, _target;

document.addEventListener('DOMContentLoaded', () => {
    acc_contents = document.querySelectorAll('.acc-content');
    acc_triggers = document.querySelectorAll('.acc-trigger');
    coll_contents = document.querySelectorAll('.coll-content');
    coll_triggers = document.querySelectorAll('.coll-triggers li');

    hide_all_acc();
    hide_all_slides();

    acc_contents[0].classList.remove('max-h-0')
    coll_contents[0].classList.remove('hide')
    coll_triggers[0].classList.add('active')

    acc_triggers.forEach((el, i) => {
        el.addEventListener('click', (evt) => {
            hide_all_acc();
            _target = evt.currentTarget.dataset.target;
            document.querySelector('#'+_target).classList.remove('max-h-0');
            setTimeout(() => {
                window.scrollTo({
                    top: document.querySelector('#'+_target).offsetTop - 48,
                    behavior: 'smooth' 
                });
            }, 250);
        });
    });

    coll_triggers.forEach((el, i) => {
        el.addEventListener('click', (evt) => {
            console.log('click')
            hide_all_slides();
            evt.currentTarget.classList.add('active')
            document.querySelector('[data-collection="'+evt.currentTarget.dataset.target+'"]').classList.remove('hide');
        })
    });

    setTimeout(() => {

        let swiper = new Swiper('.swiper', {
            lazy: true,
            navigation: {
              nextEl: ".swiper-button-next",
              prevEl: ".swiper-button-prev",
            },
            loop: true,
        });

    }, 250);

});

function hide_all_acc() {
    acc_contents.forEach((el, i) => {
        el.classList.add('max-h-0');
    });
}

function hide_all_slides() {
    coll_contents.forEach((el, i) => {
        el.classList.add('hide');
    });
    coll_triggers.forEach((el, i) => {
        el.classList.remove('active');
    });
}
