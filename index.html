<!--

    (c) 2022 PIA

-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="PIA is an SNSF-funded interdisciplinary research project involving the University of Basel, the Bern Academy of the Arts (HKB) and Cultural Anthropology Switzerland (CAS) that runs from February 2021 to January 2025.">
    <title>Participatory Image Archives</title>

    <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicons/favicon-16x16.png">
    <link rel="manifest" href="/assets/favicons/site.webmanifest">
    <link rel="mask-icon" href="/assets/favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="/assets/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="/assets/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="/assets/swiper/swiper-bundle.min.css"/>
    <link rel="stylesheet" href="/assets/normalize.css"/>
    <link rel="stylesheet" href="/assets/pia.css"/>

</head>
<body>

    <header class="relative">

        <section class="grid">
            <div class="relative flex flex-center" style="background-image: url(/assets/imgs/_DSF4628.jpg)">
                <span class="absolute uppercase">Participatory</span>
                <span>P</span>
            </div>
            <div class="relative flex flex-center" style="background-image: url(/assets/imgs/_DSF4626.jpg)">
                <span class="absolute uppercase">Image</span>
                <span>I</span>
            </div>
            <div class="relative flex flex-center" style="background-image: url(/assets/imgs/_DSF4707.jpg)">
                <span class="absolute uppercase">Archives</span>
                <span>A</span>
            </div>
        </section>

        <nav class="nav-lang absolute">
            <a class="active" href="/">EN</a>
            <a href="/de">DE</a>
            <a href="/fr">FR</a>
        </nav>

    </header>

    <main>

        <section class="grid">
            <div class="font-mono">
                <p>Participatory Knowledge Practices<br>in Analogue and Digital Image Archives</p>
                <p>02.2021–01.2025<br>Swiss National Science Foundation (SNSF), Sinergia</p>
            </div>
            <div class="col-span-2">
                <h2 class="acc-trigger uppercase cursor-pointer" data-target="Project">Project</h2>
                <div class="acc-content" data-section id="Project">
                    <p>Archives, as memory institutions, hold pieces of knowledge about everyday life and lifestyles, tradition and identity, ways of working and living in societies, as well as on institutions and individuals over time through their collections. Since the 2000s, archives have been extensively digitised, but the databases are rarely designed to enable and support large-scale use by a wide range of stakeholders. We want to make the archive a living place for generating, preserving and disseminating knowledge.</p>
                    <p>The <abbr title="Participatory Knowledge Practices in Analogue and Digital Image Archives">PIA</abbr> project connects the world of data and things in an interdisciplinary manner. We explore the phases of the analogue and digital archive from the perspectives of cultural anthropology, technology and design. By exploring participatory knowledge practices in image archives in an interdisciplinary way, we engage with processes of unfamiliar disciplines and strive for a cooperative implementation thereof. For this purpose, we develop digital tools that support contextualising, linking and contrasting images. We are dedicated to the collaboration of the scientific community and the wider public, facilitating the preservation and dissemination of knowledge, and encouraging users to engage collaboratively with their own history and contemporary practices. In a series of workshops and interviews with future users, the new requirements of digital and process-oriented knowledge production will be elaborated.</p>
                    <p>Using three collections of the photo archive of Cultural Anthropology Switzerland (<abbr title="Cultural Anthropology Switzerland">CAS</abbr>) as examples, we are developing interfaces that enable the collaborative indexing and use of archival materials. The interfaces, respectively the graphical user interface and the application programming interfaces (<abbr title="Application progamming interfaces">APIs</abbr>), provide tools and visual interfaces for the collaborative production and visualisation of knowledge with the aim of enabling a reflective and intuitive experience. Parallel to the design of the digital participatory archive, the research project investigates the phases of the analogue and digital archive from an anthropological (Institute for Cultural Anthropology and European Ethnology, University of Basel), technical (Digital Humanities Lab, University of Basel) and communicative (Bern University of the Arts) perspective.</p>
                </div>

                <h2 class="acc-trigger uppercase cursor-pointer" data-target="Accessibility">Accessibility</h2>
                <div class="acc-content" data-section id="Accessibility">
                    <p>To many, the archive may sound like a boring and highbrow academic concept. In our case, sophisticated content and attractive visualisations are being made to create a rich user experience. In this way, we complement the traditional tasks of the archive with aspects of inspiration and creation, learning and participation. We develop open interfaces and offer the possibility of expanding the archive and turning it into an instrument of current research that collects and evaluates knowledge with the involvement of other users (Citizen Science). We enable archival practices that expand an existing audience with new end users, and thus establish new networks. In order to achieve this, we explore the potential of digital access.</p>
                </div>

                <h2 class="acc-trigger uppercase cursor-pointer" data-target="Heterogeneity">Heterogeneity</h2>
                <div class="acc-content" data-section id="Heterogeneity">
                    <p>Archival records are more than just historical objects; they are invariably documents of everyday encounters, social processes and actions. Surrounding them, a network of sometimes strong, sometimes loose connections can be constructed from then until today. In this way, it becomes visible where, why and under what circumstances the objects were created, how they were handled and what path they have taken to get to and in the archive. We work on visualisations that take into account the heterogeneous character of archival materials and make their respective biographies visible. Digitised material, with its interoperable quality and new forms of visualisation, allows us to reunite what is far apart in time and space and thus to compare, analyse and cluster them.</p>
                </div>

                <h2 class="acc-trigger uppercase cursor-pointer" data-target="Materiality">Materiality</h2>
                <div class="acc-content" data-section id="Materiality">
                    <p>Digital archives raise the question of the materiality of digital objects. Digitital surrogates are merely images, copies, and limited in their dimensions and sensory experience. And yet the images on the screen always convey the material properties of the objects: they have front and back sides, inscriptions, traces, development errors, they are transparent, multi-layered or fabric-covered. They tell of their origin, their use and their peculiarities. We want to make this knowledge accessible and understandable in digital form. To this end, we also understand the necessary infrastructure involved in the creation as part of their narrative: the restoration, the relocation, the indexing, the storage devices, the research tools, the display medium, as well as the process of repro-photography.</p>
                </div>

                <h2 class="acc-trigger uppercase cursor-pointer" data-target="Interoperability">Interoperability</h2>
                <div class="acc-content" data-section id="Interoperability">
                    <p>Like all archives, the <abbr title="Cultural Anthropology Switzerland">CAS</abbr> photo archive has a wealth of metadata that can be analysed. What remains unaddressed, however, is the wealth of (historical) knowledge contained in this data, which can be continuously enriched, reflected upon and contextualised. We are creating digital means that allow different stakeholders to freely access and interact with the project's data. Both humans and machines can use, contribute to, correct and annotate the existing data in an open and interoperable manner, thus encouraging exchange and the creation of new knowledge. To do this, we use web-based standards that are widely adopted in the cultural heritage field. In addition to the digital surrogates, we also focus on how to deal with emerging digital-born records to ensure that they, too, find a proper space in the cultural heritage of the future.</p>
                </div>

                <h2 class="acc-trigger uppercase cursor-pointer" data-target="Connections">Affinities</h2>
                <div class="acc-content" data-section id="Connections">
                    <p>To be able to use an analogue archive, it requires classification systems: drawers, contact sheets, folders, files, boxes, record cards or albums. To be able to use an analogue archive, it requires classification systems: drawers, contact sheets, folders, files, boxes, record cards or albums. Each archival record raises questions about the extent to which a similar connection, a completely different one, or even no relation at all is created in and with it and other objects. In turn, new affinities develop through digital means, which encourage us to see the familiar in a new way, but also bear an inherent element of arbitrariness. Data models and pattern recognition can uncover semantic relationships between entities that were previously incomplete or difficult for users to access. Using specific interfaces and visualisations, we make it possible to explore digital assets and discover forms of relationships and similarities between images. In doing so, we also deal with questions of spatial, temporal or topic-based connections and, accordingly, with missing links and blind spots.</p>
                </div>

                <h2 class="acc-trigger uppercase cursor-pointer" data-target="Artificial-Intelligence">Artificial Intelligence</h2>
                <div class="acc-content" data-section id="Artificial-Intelligence">
                    <p>The cataloguing and indexing of photographic collections is a time-consuming and complex process. The resulting metadata, however, is fundamental for any database use. Artificial intelligence (<abbr title="Articifial Intelligence">AI</abbr>) methods for image analysis can support us in this work. We facilitate automated searches for simple image attributes such as colour, shapes and localisation of image components. It should also become possible to recognise texts and object types for extracting metadata. Finally, a number of experimental techniques will be developed to establish previously unrecognised relationships between objects. We will demonstrate how <abbr title="Artificial Intelligence">AI</abbr> works in machine learning and make transparent to users how the use of AI is shaping and will continue to change our understanding of archival practices. This step is expected to be more than just feedback, but rather to open up a dialogue about digital image content.</p>
                </div>

                <h2 class="acc-trigger uppercase cursor-pointer" data-target="Bias-Management">Bias Management</h2>
                <div class="acc-content" data-section id="Bias-Management">
                    <p>Historical photo collections only contain what was considered worthy of depiction, representation and preservation at a particular point of time. The associated metadata was man-made and thus is never objective. Collections and their metadata reflect biases or focus narrowly on selected areas and perceptions. Machines working on the basis of such data automatically reproduce the implicit biases in decision-making due to so-called biased algorithms. Therefore, understanding the data used for training and the algorithms applied for decision-making is crucial to ensure the integrity of the application of these technologies in archives. We take ethical issues into account when using <abbr title="Artificial Intelligence">AI</abbr> and visualisations, because the higher the awareness of a possible bias, the faster it can be detected or brought up for consideration with users.</p>
                </div>
            </div>
        </section>
        <section class="bg-gray collections">
            <div class="grid">
                <div class="font-mono">
                    <p>Explore our archive collections</p>
                </div>
                <div class="col-span-2">
                    <ul class="coll-triggers">
                        <li class="active" data-target="collection-1"><h2 class="uppercase">Ernst Brunner</h2></li>
                        <li data-target="collection-2"><h2 class="uppercase">Familie Kreis</h2></li>
                        <li data-target="collection-3"><h2 class="uppercase">Atlas der Schweizerischen Volkskunde</h2></li>
                    </ul>
                </div>
            </div>
            <div>
                <div class="coll-content swiper" data-collection="collection-1">
                    <div class="swiper-wrapper">
                        <figure class="swiper-slide">
                            <img class="m-auto" src="/assets/imgs/SGV_12/_DSF4617.jpg" alt="">
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_12/_DSF4625.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_12/_DSF4626.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_12/IMG_0224.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_12/IMG_0640.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_12/IMG_2583.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_12/IMG_2639.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_12/IMG_2640.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_12/IMG_2653.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_12/IMG_2715.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_12/IMG_2716.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_12/IMG_3011.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_12/IMG_3037.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_12/IMG_9550.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_12/IMG_9792.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_12/SGV_12N_43191.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_12/verzeichnis_006.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                    </div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
                <div class="coll-content swiper" data-collection="collection-2">
                    <div class="swiper-wrapper">
                        <figure class="swiper-slide">
                            <img class="m-auto" src="/assets/imgs/SGV_10/10A_04.jpg" alt="">
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_10/20210628_151752.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_10/Bildschirmfoto 2022-02-22 um 17.07.23.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_10/Bildschirmfoto 2022-02-22 um 17.09.36.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_10/Bildschirmfoto 2022-02-28 um 09.56.28.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_10/Bildschirmfoto 2022-05-03 um 17.36.16.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_10/DSC00633.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_10/SGV_10A_00001.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_10/SGV_10A_Auswahl lose Fotos_Schachtel.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_10/SGV_10N_00745.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_10/SGV_10P_00127.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_10/SGV_10P_00281.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_10/SGV_10P_00305 .jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_10/SGV_10P_01503.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                    </div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
                <div class="coll-content swiper" data-collection="collection-3">
                    <div class="swiper-wrapper">
                        <figure class="swiper-slide">
                            <img class="m-auto" src="/assets/imgs/SGV_05/_DSF4630-sw.jpg" alt="">
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_05/Atlas_Kartekarte.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_05/Beispiel_Umschlag_2.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_05/Beispiel_Umschlag_10.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_05/DSC_2358.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_05/DSC00719_Frage94_Ort107__r.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_05/DSC00721.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_05/DSC00723_Frage94_Ort107__r.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_05/DSC00725.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_05/DSC01049.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_05/Frage7_Ort208.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_05/Frage7_Ort386_1r.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_05/Frage94_Ort90_Zeichnung.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_05/oben_DSC01044.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_05/Originalaufnahmen_1Cartigny_4.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_05/Originalaufnahmen_2Genève_4.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_05/Originalkarten_Karte186_Frage94.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_05/SGV_05P_00095.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                        <figure class="swiper-slide">
                            <img class="m-auto swiper-lazy" data-src="/assets/imgs/SGV_05/SGV_05P_00115.jpg" alt="">
                            <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                        </figure>
                    </div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>
        </section>
        <section class="grid">
            <div class="font-mono">
                <p class="mb-2">A collaborative research project by University of Basel, University of Bern Bern, Academy of Arts Bern, and Cultural Anthropology Switzerland</p>
                <p class="mb-10">Funded by the Swiss National Science Foundation (<abbr title="Swiss National Science Foundation">SNSF</abbr>), Stiftung Ernst Göhner, Memoriav, Stiftung für Kunst, Kultur &amp; Geschichte,  Jacqueline Spengler Stiftung (<abbr title="Christop Merian Stiftung">CMS</abbr>)</p>
            </div>
            <div class="col-span-2">
                <h2 class="acc-trigger uppercase cursor-pointer" data-target="Team">Team</h2>
                <div class="acc-content" data-section id="Team">
                    <h3>Project Management</h3>
                    <ul>
                        <li>Prof. Dr. Walter Leimgruber, lead subproject Cultural Anthropology, dissertation supervisor, University of Basel</li>
                        <li>PD Dr. Peter Fornaro, lead subproject Digital Humanities, dissertation supervisor, University of Basel</li>
                        <li>Dr. Ulrike Felsing, lead subproject Design Research, dissertation supervisor, University of the Arts Bern</li>
                    </ul>

                    <h3>Project Partners & Dissertation Supervisors</h3>
                    <ul>
                        <li>Prof. Dr. Tobias Hodel, Walter Benjamin Kolleg, Digital Humanities, University of Bern</li>
                        <li>Prof. Dr. Heiko Schuldt, Department of Mathematics and Computer Science, University of Basel</li>
                    </ul>

                    <h3>Researchers</h3>
                    <ul>
                        <li>Dr. Vera Chiquet, Postdoctoral Researcher, Digital Humanities Lab, University of Basel</li>
                        <li>Adrian Demleitner, Software Developer, Digital Humanities Lab, University of Basel</li>
                        <li>Fabian Frei, Software Developer, Digital Humanities Lab, University of Basel</li>
                        <li>Dr. Nicole Peduzzi, Supervision of the restoration &amp; digitization of the photographs, University of Basel/Cultural Anthropology Switzerland</li>
                        <li>Christoph Rohrer, Software Developer, Digital Humanities Lab, University of Basel</li>
                        <li>Daniel Schoeneck, Research Fellow, University of the Arts Bern</li>
                    </ul>

                    <h3>Doctoral Students</h3>
                    <ul>
                        <li>Murielle Cornut, Cultural Anthropology, University of Basel</li>
                        <li>Max Frischknecht, Digital Humanities, University of Bern/University of the Arts Bern</li>
                        <li>Birgit Huber, Cultural Anthropology, University of Basel</li>
                        <li>Fabienne Lüthi, Cultural Anthropology, University of Basel</li>
                        <li>Julien A. Raemy, Digital Humanities Lab, University of Basel</li>
                        <li>Florian Spiess, Department of Mathematics and Computer Science, University of Basel</li>
                    </ul>

                    <h3>External Partners</h3>
                    <ul>
                        <li>Regula Anklin, Conservation and restoration of historical and modern photographs, Atelier für Restaurierung Anklin &amp; Assen</li>
                        <li>Lukas Zimmer, External design agency, A/Z&amp;T Astrom / Zimmer &amp; Tereszkiewicz</li>
                        <li>Dr. Martin Stuber, Institute of History, University of Bern</li>
                    </ul>
                </div>
                <h2 class="acc-trigger uppercase cursor-pointer" data-target="Partner">Partner</h2>
                <div class="acc-content" data-section id="Partner">
                    <ul>
                        <li>Cultural Anthropology Switzerland (formerly <i>Swiss Society for Folklore Studies</i>)</li>
                        <li>Universität Basel Digital Humanities Lab, Departement Mathematik und Informatik und Seminar für Kulturwissenschaft und Europäische Ethnologie</li>
                        <li>Universität Bern Digital Humanities</li>
                        <li>Hochschule der Künste Bern Institute of Design Research</li>
                        <li>DaSCH, Swiss National Data &amp; Service Center for the Humanities</li>
                        <li>Büro für Fotografiegeschichte Bern</li>
                        <li>ETH Bibliothek Zürich</li>
                        <li>Fotostiftung Schweiz, Winterthur</li>
                        <li>Infoclio, Bern</li>
                        <li>Kunsthalle, Basel</li>
                        <li>Memoriav, Verein zur Erhaltung des audiovisuellen Kulturgutes der Schweiz, Bern</li>
                        <li>Museum der Kulturen, Basel</li>
                        <li>Ringier Fotoarchiv</li>
                        <li>Schweizerisches Landesmuseum, Zürich</li>
                        <li>Staatsarchiv Aargau</li>
                        <li>Staatsarchiv Basel-Stadt</li>
                        <li>Schweizerischer Nationalfonds</li>
                        <li>Büro für Fotografiegeschichte Bern</li>
                    </ul>
                </div>
                <h2 class="acc-trigger uppercase cursor-pointer" data-target="Contact">Contact</h2>
                <div class="acc-content" data-section id="Contact">
                    <p>
                        Fotoarchiv der Empirischen Kulturwissenschaft Schweiz<br>
                        Sinergia Projekt<br>
                        Spalenvorstadt 2<br>
                        CH-4051 Basel<br><br>

                        <a class="hover:underline" href="mailto:archiv(at)sgv-sstp.ch">archiv(at)sgv-sstp.ch</a><br>
                        <a class="hover:underline" href="tel:0041612076497">+41 61 207 64 97</a><br>
                    </p>
                </div>

            </div>
        </section>


    </main>

    <footer class="bg-gray">
        <p class="flex logos">
            <a href="https://www.unibas.ch/en" target="_blank"></a><img class="mr-6" src="/assets/logos/UniBas_Logo_EN_Schwarz_Trans_RGB_65.svg" alt="University of Basel">
            <a href="https://www.unibe.ch/index_eng.html" target="_blank" class="logo-ub"><img src="/assets/logos/university_bern.svg" alt="Universität Bern"></a>
            <a href="https://www.hkb.bfh.ch/en/" target="_blank" class="logo-hkb"><img src="/assets/logos/HKB_Markenzeichen_Kooperation_RGB_Schwarz.svg" alt="Hochschule der Künste Bern"></a>
            <a href="https://www.bfh.ch/en/" target="_blank"><img src="/assets/logos/bfh.svg" alt="Berner Fachhochschule"></a>
            <a href="https://data.snf.ch/grants/grant/193788" target="_blank"><img src="/assets/logos/SNF_logo_standard_web_sw_pos_e.svg" alt="SNSF"></a>
            <a href="https://archiv.sgv-sstp.ch/" target="_blank"><img class="bg-white" src="/assets/logos/ekws-logo.svg" alt="EKWS"></a>
        </p>
    </footer>

    <script type="text/javascript" src="/assets/swiper/swiper-bundle.min.js"></script>
    <script type="text/javascript" src="/assets/pia.js"></script>
    
</body>
</html>
